<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function index()
	{
		$this->load->view('beranda');
	}
	public function about()
	{
		$this->load->view('about');
	}
	
	public function kontak()
	{
		$this->load->view('kontak');
	}
	public function login()
	{
		$this->load->view('login');
	}
}
